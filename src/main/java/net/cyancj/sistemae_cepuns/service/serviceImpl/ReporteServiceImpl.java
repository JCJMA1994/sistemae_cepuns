package net.cyancj.sistemae_cepuns.service.serviceImpl;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;


import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
@Service
public class ReporteServiceImpl {

    @Autowired
    @Qualifier("jdbcTemplate")
    private JdbcTemplate jdbcTemplate;


    public JasperPrint reporteMatricula() throws JRException, SQLException {

            Connection conn = Objects.requireNonNull(jdbcTemplate.getDataSource()).getConnection();
            InputStream reporteMatriculaStream
                    = getClass().getResourceAsStream("/reportes/reporte_total_matriculas.jrxml");
            JasperReport jasperReport
                    = JasperCompileManager.compileReport(reporteMatriculaStream);
        Map<String, Object> parameters = new HashMap<String, Object>();
        return JasperFillManager.fillReport(jasperReport, parameters, conn);
    }


    public JasperPrint reporteMatriculaCiclo(Long id_ciclo,String nombre_ciclo) throws JRException, SQLException {

        Connection conn = Objects.requireNonNull(jdbcTemplate.getDataSource()).getConnection();
        InputStream reporteMatriculaCicloStream
                = getClass().getResourceAsStream("/reportes/reporte_matricula_ciclo.jrxml");
        JasperReport jasperReport
                = JasperCompileManager.compileReport(reporteMatriculaCicloStream);
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("ciclo_nombre",nombre_ciclo);
        parameters.put("id_cicloo",id_ciclo);
        System.out.println(id_ciclo);
        return JasperFillManager.fillReport(jasperReport, parameters, conn);
    }
}
