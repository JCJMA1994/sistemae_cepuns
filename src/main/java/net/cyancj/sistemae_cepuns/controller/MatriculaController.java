package net.cyancj.sistemae_cepuns.controller;

import net.cyancj.sistemae_cepuns.entity.Alumno;
import net.cyancj.sistemae_cepuns.entity.Ciclo;
import net.cyancj.sistemae_cepuns.entity.Matricula;
import net.cyancj.sistemae_cepuns.service.IAlumnoService;
import net.cyancj.sistemae_cepuns.service.ICicloService;
import net.cyancj.sistemae_cepuns.service.IMatriculaService;
import net.cyancj.sistemae_cepuns.service.serviceImpl.ReporteServiceImpl;
import net.cyancj.sistemae_cepuns.utils.JsonResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

@RestController
@RequestMapping("/matricula")
public class MatriculaController {

    public IMatriculaService matriculaService;
    public IAlumnoService alumnoService;
    public ReporteServiceImpl reporteService;
    public ICicloService cicloService;

    public MatriculaController(IMatriculaService matriculaService, IAlumnoService alumnoService, ReporteServiceImpl reporteService, ICicloService cicloService) {
        this.matriculaService = matriculaService;
        this.alumnoService = alumnoService;
        this.reporteService = reporteService;
        this.cicloService = cicloService;
    }

    @PostMapping("/registrar")
    public String registrarMatricula(@Valid @ModelAttribute("matricula") Matricula matricula, BindingResult bindingResult) {
        JsonResponse json = new JsonResponse();
        if (!bindingResult.hasErrors()) {
            if (!matriculaService.searchByName(matricula)) {
                Alumno alumno = matricula.getAlumno();
                if (!alumnoService.searchByName(alumno)) {
                    alumnoService.add(alumno);
                }
                matriculaService.add(matricula);
                json.setMessage("Matricula registrado exitosamente");
                json.success();
            } else {
                json.setMessage("Existe una matricula registrada");
            }
        } else {
            json.setMessage("Ocurrió un error");
        }

        return json.getJson();
    }

    @PostMapping("/reporte/total")
    public void reporteTotal(HttpServletResponse response) throws IOException, SQLException, JRException {
        JasperPrint jasperPrint = null;
        OutputStream out = response.getOutputStream();
        jasperPrint = reporteService.reporteMatricula();
        JasperExportManager.exportReportToPdfStream(jasperPrint, out);
    }

    @PostMapping("/reporte/ciclo/{id}")
    public void reporteCiclo(@PathVariable("id") Long id, HttpServletResponse response) throws IOException, SQLException, JRException {
        Ciclo ciclo = cicloService.search(id);
        if (ciclo != null) {
            String nombre_ciclo = ciclo.getAnio() + "-" +ciclo.getN_ciclo();
            JasperPrint jasperPrint = null;
            OutputStream out = response.getOutputStream();
            jasperPrint = reporteService.reporteMatriculaCiclo(id,nombre_ciclo);
            JasperExportManager.exportReportToPdfStream(jasperPrint, out);
        }
    }

}
