<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core_1_1" %>
<%--
  Created by IntelliJ IDEA.
  User: CyanCJ
  Date: 9/09/2020
  Time: 05:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<h3 class="text-center text-dark text-uppercase" style="font-weight: 600;">Registros por Ciclo</h3>
<br>
<div class="row">
    <div class="col-md-8" style="font-size: 1.4em;">
        <span>CICLO:</span> ${info_ciclo.anio}-${info_ciclo.id}
    </div>
    <div class="col-md-4">
        <button class="btn btn-sm btn-outline-success btn-sm btn-reportes text-uppercase" style="margin-left: 85px" onclick="reporte_matricula_ciclo(${info_ciclo.id})">Generar Reporte</button>
    </div>
</div>

<table class="table table-striped table-bordered">
    <thead class="thead-dark text-uppercase">
    <tr class=" table-sm  d-flex">
        <th style="font-size: .9em; width: 40px;">#</th>
        <th  style="font-size: .9em;width:90px;">DNI</th>
        <th class="col-md-4"  style="font-size: .8em;">Apellidos y Nombres</th>
        <th class="col-md-4"  style="font-size: .8em;">Escuela</th>
        <th class="col-md-2" style="font-size: .75em;">N° de voucher</th>
    </tr>
    </thead>
    <tbody class="table-sm table-text-small">
    <core:forEach var="matricula" items="${matriculas}">
        <tr class="d-flex">
            <td style="width: 40px;" >${matricula.id}</td>
            <td style="width:90px;">${matricula.alumno.dni}</td>
            <td class="col-md-4 text-left" >${matricula.alumno.apellidos} ${matricula.alumno.nombres}</td>
            <td class="col-md-4 text-left">${matricula.carrera.nombre_carrera}</td>
            <td class="col-md-2">${matricula.codigo_voucher}</td>
        </tr>
    </core:forEach>
    </tbody>
</table>