<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core_1_1" %>
<%--
  Created by IntelliJ IDEA.
  User: CyanCJ
  Date: 9/09/2020
  Time: 05:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<h3 class="text-center text-dark text-uppercase" style="font-weight: 600;">Registros Generales</h3>
<br>
<div class="offset-md-8 col-md-4">
    <button class="btn btn-sm btn-outline-success btn-sm btn-reportes text-uppercase" style="margin-left: 85px" onclick="reporte_matricula_total()">Generar Reporte</button>
</div>
<table class="table table-striped table-bordered">
    <thead class=" table-sm thead-dark text-uppercase">
    <tr class="d-flex"  >
        <th style="font-size: .9em; width: 40px;">#</th>
        <th  style="font-size: .9em;width:75px;">DNI</th>
        <th style="font-size: .9em;width:275px;">Apellidos y Nombres</th>
        <th class="col-md-4" style="font-size: .9em;">Escuela</th>
        <th class="col-md-1" style="font-size: .9em;">Ciclo</th>
        <th class="col-md-1" style="font-size: .7em;">N° voucher</th>
    </tr>
    </thead>
    <tbody class="table-sm table-text-small">
    <core:forEach var="matricula" items="${matriculas}">
        <tr class="d-flex">
            <td style="width: 40px;">${matricula.id}</td>
            <td class="text-left" style="width:75px;" >${matricula.alumno.dni}</td>
            <td class="text-left" style="width:275px">${matricula.alumno.apellidos} ${matricula.alumno.nombres}</td>
            <td class="col-md-4 text-left">${matricula.carrera.nombre_carrera}</td>
            <td class="col-md-1">${matricula.ciclo.anio}-${matricula.ciclo.n_ciclo}</td>
            <td class="col-md-1">${matricula.codigo_voucher}</td>
        </tr>
    </core:forEach>
    </tbody>
</table>